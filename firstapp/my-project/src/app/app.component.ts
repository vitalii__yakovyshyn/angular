import { Component } from '@angular/core';
import {DataService} from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})
export class AppComponent {
  title = 'my-project';
    
  items: Date[] = [];
  myDate: Date =  new Date();
  constructor(private dataService: DataService){}
    
  addItem(myDate: Date){
        
      this.dataService.addData(new Date());
  }

  ngOnInit(){
      this.items = this.dataService.getData();
  }
}
