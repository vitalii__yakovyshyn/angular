export class DataService{
  
    private data: Date[] = [];
      
    getData(): Date[] {
          
        return this.data;
    }
   
    addData(myDate: Date){
          
        this.data.push(myDate);
    }
    
    
}